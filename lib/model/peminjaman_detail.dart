class Peminjaman_detail {
  String? kode_pengajuan;
  String? kode_barang;
  String? jumlah;

  Peminjaman_detail({
    this.kode_pengajuan,
    this.kode_barang,
    this.jumlah,
  });

  factory Peminjaman_detail.fromJson(Map<String, dynamic> json) =>
      Peminjaman_detail(
        kode_pengajuan: json['kode_pengajuan'],
        kode_barang: json['kode_barang'],
        jumlah: json['jumlah'],
      );

  Map<String, dynamic> toJson() => {
        'kode_pengajuan': this.kode_pengajuan,
        'kode_barang': this.kode_barang,
        'jumlah': this.jumlah,
      };
}
