class Pengembalian_detail {
  String? kode_pengembalian_detail;
  String? kode_pengembalian;
  String? kode_barang;
  String? jumlah;

  Pengembalian_detail(
      {this.kode_pengembalian_detail,
      this.kode_pengembalian,
      this.kode_barang,
      this.jumlah});

  factory Pengembalian_detail.fromJson(Map<String, dynamic> json) =>
      Pengembalian_detail(
          kode_pengembalian_detail: json['kode_pengembalian_detail'],
          kode_pengembalian: json['kode_pengembalian'],
          kode_barang: json['kode_barang'],
          jumlah: json['jumlah']);

  Map<String, dynamic> toJson() => {
        'kode_pengembalian_detail': this.kode_pengembalian_detail,
        'kode_pengembalian': this.kode_pengembalian,
        'kode_barang': this.kode_barang,
        'jumlah': this.jumlah,
      };
}
