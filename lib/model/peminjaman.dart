class Peminjaman {
  String? npm_peminjam;
  String? kode_pengajuan;
  String? tanggal;
  String? nama_peminjam;
  String? prodi;

  Peminjaman(
      {this.npm_peminjam,
      this.kode_pengajuan,
      this.tanggal,
      this.nama_peminjam,
      this.prodi});

  factory Peminjaman.fromJson(Map<String, dynamic> json) => Peminjaman(
      npm_peminjam: json['npm_peminjam'],
      kode_pengajuan: json['kode_pengajuan'],
      tanggal: json['tanggal'],
      nama_peminjam: json['nama_peminjam'],
      prodi: json['prodi']);

  Map<String, dynamic> toJson() => {
        'npm_peminjaman': this.npm_peminjam,
        'kode_pengajuan': this.kode_pengajuan,
        'tanggal': this.tanggal,
        'nama_peminjam': this.nama_peminjam,
        'prodi': this.prodi
      };
}
