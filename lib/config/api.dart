class Api {
  static const _host = "http://192.168.100.143/api_kelas-main";

  static String _user = "$_host/user";
  static String _mahasiswa = "$_host/mahasiswa";
  static String _barang = "$_host/barang";
  static String _peminjaman = "$_host/peminjaman";
  static String _peminjaman_detail = "$_host/peminjaman_detail";
  static String _pengembalian = "$_host/pengembalian";
  static String _pengembalian_detail = "$_host/pengembalian_detail";

  static String login = "$_host/login.php";

  // user
  static String addUser = "$_user/add_user.php";
  static String deleteUser = "$_user/delete_user.php";
  static String getUsers = "$_user/get_users.php";
  static String updateUser = "$_user/update_user.php";

  // mahasiswa
  static String addMahasiswa = "$_mahasiswa/add_mahasiswa.php";
  static String deleteMahasiswa = "$_mahasiswa/delete_mahasiswa.php";
  static String getMahasiswa = "$_mahasiswa/get_mahasiswa.php";
  static String updateMahasiswa = "$_mahasiswa/update_mahasiswa.php";

  //barang
  static String addBarang = "$_barang/add_barang.php";
  static String deleteBarang = "$_barang/delete_barang.php";
  static String getBarang = "$_barang/get_barang.php";
  static String updateBarang = "$_barang/update_barang.php";

  //peminjaman
  static String addPeminjaman = "$_peminjaman/add_peminjaman.php";
  static String deletePeminjaman = "$_peminjaman/delete_peminjaman.php";
  static String getPeminjaman = "$_peminjaman/get_peminjaman.php";
  static String updatePeminjaman = "$_peminjaman/update_peminjaman.php";

  //peminjaman_detail
  static String addPeminjaman_detail =
      "$_peminjaman_detail/add_peminjaman_detail.php";
  static String deletePeminjaman_detail =
      "$_peminjaman_detail/delete_peminjaman_detail.php";
  static String getPeminjaman_detail =
      "$_peminjaman_detail/get_peminjaman_detail.php";
  static String updatePeminjaman_detail =
      "$_peminjaman_detail/update_peminjaman_detail.php";

  //pengembalian
  static String addPengembalian = "$_pengembalian/add_pengembalian.php";
  static String deletePengembalian = "$_pengembalian/delete_pengembalian.php";
  static String getPengembalian = "$_pengembalian/get_pengembalian.php";
  static String updatePengembalian = "$_pengembalian/update_pengembalian.php";

  //pengembalian_detail
  static String addPengembalian_detail =
      "$_pengembalian_detail/add_pengembalian_detail.php";
  static String deletePengembalian_detail =
      "$_pengembalian_detail/delete_pengembalian_detail.php";
  static String getPengembalian_detail =
      "$_pengembalian_detail/get_pengembalian_detail.php";
  static String updatePengembalian_detail =
      "$_pengembalian_detail/update_pengembalian_detail.php";
}
