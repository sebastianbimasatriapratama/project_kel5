import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Asset {
  static Color colorPrimaryDark = Color.fromARGB(255, 115, 18, 32);
  static Color colorPrimary = Color.fromARGB(255, 255, 217, 0);
  static Color colorSecondary = Colors.lightBlue;
  static Color colorAccent = Color.fromARGB(255, 115, 18, 32);
}
