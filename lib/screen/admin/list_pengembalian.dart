import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:project_kelas/config/asset.dart';
import 'package:project_kelas/event/event_db.dart';
import 'package:project_kelas/model/pengembalian.dart';
import 'package:project_kelas/screen/admin/add_update_pengembalian.dart';
import 'package:project_kelas/screen/admin/add_update_pengembalian_detail.dart';
import 'package:project_kelas/screen/admin/list_pengembalian_detail.dart';
// import 'package:project_kelas/screen/admin/add_update_pengembalian.dart';

import '../../model/pengembalian.dart';

class ListPengembalian extends StatefulWidget {
  @override
  State<ListPengembalian> createState() => _ListPengembalianState();
}

class _ListPengembalianState extends State<ListPengembalian> {
  List<Pengembalian> _listPengembalian = [];

  void getPengembalian() async {
    _listPengembalian = await EventDb.getPengembalian();

    setState(() {});
  }

  @override
  void initState() {
    getPengembalian();
    super.initState();
  }

  void showOption(Pengembalian? pengembalian) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdatePengembalian(pengembalian: pengembalian))
            ?.then((value) => getPengembalian());
        break;
      case 'delete':
        EventDb.deletePengembalian(pengembalian!.kode_pengembalian!)
            .then((value) => getPengembalian());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // titleSpacing: 0,
        title: Text('List Pengembalian'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          _listPengembalian.length > 0
              ? ListView.builder(
                  itemCount: _listPengembalian.length,
                  itemBuilder: (context, index) {
                    Pengembalian pengembalian = _listPengembalian[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(pengembalian.kode_pengembalian ?? ''),
                      subtitle: Text(pengembalian.tanggal_kembali ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(pengembalian),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
              bottom: 16,
              left: 350,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  FloatingActionButton(
                    onPressed: () => Get.to(AddUpdatePengembalian())
                        ?.then((value) => getPengembalian()),
                    child: Icon(Icons.add),
                    backgroundColor: Asset.colorAccent,
                  ),
                  SizedBox(height: 8),
                  ElevatedButton(
                    onPressed: () {
                      Get.off(ListPengembalianDetail());
                    },
                    style: ElevatedButton.styleFrom(
                      primary: Asset.colorPrimaryDark,
                    ),
                    child: Text('pengembalian_detail'),
                  ),
                ],
              ))
        ],
      ),
    );
  }
}
