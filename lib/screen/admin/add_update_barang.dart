import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:project_kelas/config/asset.dart';
import 'package:project_kelas/event/event_db.dart';
import 'package:project_kelas/screen/admin/list_barang.dart';
import 'package:project_kelas/widget/info.dart';

import '../../model/barang.dart';

class AddUpdateBarang extends StatefulWidget {
  final Barang? barang;
  AddUpdateBarang({this.barang});

  @override
  State<AddUpdateBarang> createState() => _AddUpdateBarangState();
}

class _AddUpdateBarangState extends State<AddUpdateBarang> {
  var _formKey = GlobalKey<FormState>();
  var _controllerkode_barang = TextEditingController();
  var _controllernama_barang = TextEditingController();
  var _controllerjumlah = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.barang != null) {
      _controllerkode_barang.text = widget.barang!.kode_barang!;
      _controllernama_barang.text = widget.barang!.nama_barang!;
      _controllerjumlah.text = widget.barang!.jumlah!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.barang != null
            ? Text('Update Barang')
            : Text('Tambah Barang'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.barang == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkode_barang,
                  decoration: InputDecoration(
                      labelText: "kode barang",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllernama_barang,
                  decoration: InputDecoration(
                      labelText: "Nama barang",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerjumlah,
                  decoration: InputDecoration(
                      labelText: "Jumlah",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.barang == null) {
                        String message = await EventDb.AddBarang(
                          _controllerkode_barang.text,
                          _controllernama_barang.text,
                          _controllerjumlah.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerkode_barang.clear();
                          _controllernama_barang.clear();
                          _controllerjumlah.clear();
                          Get.off(
                            ListBarang(),
                          );
                        }
                      } else {
                        EventDb.UpdateBarang(
                          _controllerkode_barang.text,
                          _controllernama_barang.text,
                          _controllerjumlah.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.barang == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
