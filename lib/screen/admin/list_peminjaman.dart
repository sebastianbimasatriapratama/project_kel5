import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_kelas/config/asset.dart';
import 'package:project_kelas/event/event_db.dart';
import 'package:project_kelas/model/peminjaman.dart';
import 'package:project_kelas/screen/admin/add_update_peminjaman.dart';
import 'package:project_kelas/screen/admin/list_peminjaman_detail.dart';
// import 'package:project_kelas/screen/admin/add_update_peminjaman.dart';

class ListPeminjaman extends StatefulWidget {
  @override
  State<ListPeminjaman> createState() => _ListPeminjamanState();
}

class _ListPeminjamanState extends State<ListPeminjaman> {
  List<Peminjaman> _listPeminjaman = [];

  void getPeminjaman() async {
    _listPeminjaman = await EventDb.getPeminjaman();

    setState(() {});
  }

  @override
  void initState() {
    getPeminjaman();
    super.initState();
  }

  void showOption(Peminjaman? peminjaman) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdatePeminjaman(peminjaman: peminjaman))
            ?.then((value) => getPeminjaman());
        break;
      case 'delete':
        EventDb.deletePeminjaman(peminjaman!.npm_peminjam!)
            .then((value) => getPeminjaman());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // titleSpacing: 0,
        title: Text('List Peminjaman'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          _listPeminjaman.length > 0
              ? ListView.builder(
                  itemCount: _listPeminjaman.length,
                  itemBuilder: (context, index) {
                    Peminjaman peminjaman = _listPeminjaman[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(peminjaman.nama_peminjam ?? ''),
                      subtitle: Text(peminjaman.npm_peminjam ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(peminjaman),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
              bottom: 16,
              left: 350,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  FloatingActionButton(
                    onPressed: () => Get.to(AddUpdatePeminjaman())
                        ?.then((value) => getPeminjaman()),
                    child: Icon(Icons.add),
                    backgroundColor: Asset.colorAccent,
                  ),
                  SizedBox(height: 8),
                  ElevatedButton(
                    onPressed: () {
                      Get.off(ListPeminjamanDetail());
                    },
                    style: ElevatedButton.styleFrom(
                      primary: Asset.colorPrimaryDark,
                    ),
                    child: Text('peminjaman_detail'),
                  ),
                ],
              ))
        ],
      ),
    );
  }
}
