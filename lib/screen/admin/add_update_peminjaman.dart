import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:project_kelas/config/asset.dart';
import 'package:project_kelas/event/event_db.dart';
import 'package:project_kelas/screen/admin/list_peminjaman.dart';
import 'package:project_kelas/widget/info.dart';

import '../../model/peminjaman.dart';

class AddUpdatePeminjaman extends StatefulWidget {
  final Peminjaman? peminjaman;
  AddUpdatePeminjaman({this.peminjaman});

  @override
  State<AddUpdatePeminjaman> createState() => _AddUpdatePeminjamanState();
}

class _AddUpdatePeminjamanState extends State<AddUpdatePeminjaman> {
  var _formKey = GlobalKey<FormState>();
  var _controllernpm_peminjam = TextEditingController();
  var _controllerkode_peminjaman = TextEditingController();
  var _controllertanggal = TextEditingController();
  var _controllernama_peminjam = TextEditingController();
  var _controllerprodi = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.peminjaman != null) {
      _controllernpm_peminjam.text = widget.peminjaman!.npm_peminjam!;
      _controllerkode_peminjaman.text = widget.peminjaman!.kode_pengajuan!;
      _controllertanggal.text = widget.peminjaman!.tanggal!;
      _controllernama_peminjam.text = widget.peminjaman!.nama_peminjam!;
      _controllerprodi.text = widget.peminjaman!.prodi!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.peminjaman != null
            ? Text('Update Peminjaman')
            : Text('Tambah Peminjaman'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.peminjaman == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllernpm_peminjam,
                  decoration: InputDecoration(
                      labelText: "npm_peminjam",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkode_peminjaman,
                  decoration: InputDecoration(
                      labelText: "kode_pengajuan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllertanggal,
                  decoration: InputDecoration(
                      labelText: "tanggal",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllernama_peminjam,
                  decoration: InputDecoration(
                      labelText: "nama_peminjam",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerprodi,
                  decoration: InputDecoration(
                      labelText: "Prodi",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.peminjaman == null) {
                        String message = await EventDb.AddPeminjaman(
                          _controllernpm_peminjam.text,
                          _controllerkode_peminjaman.text,
                          _controllertanggal.text,
                          _controllernama_peminjam.text,
                          _controllerprodi.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllernpm_peminjam.clear();
                          _controllerkode_peminjaman.clear();
                          _controllertanggal.clear();
                          _controllernama_peminjam.clear();
                          _controllerprodi.clear();
                          Get.off(
                            ListPeminjaman(),
                          );
                        }
                      } else {
                        EventDb.UpdatePeminjaman(
                          _controllernpm_peminjam.text,
                          _controllerkode_peminjaman.text,
                          _controllertanggal.text,
                          _controllernama_peminjam.text,
                          _controllerprodi.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.peminjaman == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
