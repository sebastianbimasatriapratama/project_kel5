import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:project_kelas/config/asset.dart';
import 'package:project_kelas/event/event_db.dart';
import 'package:project_kelas/model/peminjaman_detail.dart';
import 'package:project_kelas/model/pengembalian_detail.dart';
import 'package:project_kelas/screen/admin/list_peminjaman_detail.dart';
import 'package:project_kelas/screen/admin/list_peminjaman.dart';
import 'package:project_kelas/widget/info.dart';
import '../../model/peminjaman.dart';

class AddUpdatePeminjamanDetail extends StatefulWidget {
  final Peminjaman_detail? peminjaman_detail;
  AddUpdatePeminjamanDetail({this.peminjaman_detail});

  @override
  State<AddUpdatePeminjamanDetail> createState() => _AddUpdatePeminjamanDetailState();
}

class _AddUpdatePeminjamanDetailState extends State<AddUpdatePeminjamanDetail> {
  var _formKey = GlobalKey<FormState>();
  var _controllerkodePengajuan = TextEditingController();
  var _controllerkodeBarang = TextEditingController();
  var _controllerjumlah = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.peminjaman_detail != null) {
      _controllerkodePengajuan.text = widget.peminjaman_detail!.kode_pengajuan!;
      _controllerkodeBarang.text = widget.peminjaman_detail!.kode_barang!;
      _controllerjumlah.text = widget.peminjaman_detail!.jumlah!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.peminjaman_detail != null
            ? Text('Update Detail Peminjaman')
            : Text('Tambah Detail Peminjaman'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.peminjaman_detail == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkodePengajuan,
                  decoration: InputDecoration(
                      labelText: "Kode Pengajuan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),       
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkodeBarang,
                  decoration: InputDecoration(
                      labelText: "Kode Barang",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerjumlah,
                  decoration: InputDecoration(
                      labelText: "Jumlah",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.peminjaman_detail == null) {
                        String message = await EventDb.AddPeminjamanDetail(
                          _controllerkodePengajuan.text,
                          _controllerkodeBarang.text,
                          _controllerjumlah.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerkodePengajuan.clear();
                          _controllerkodeBarang.clear();
                          _controllerjumlah.clear();
                          Get.off(
                            ListPeminjamanDetail(),
                          );
                        }
                      } else {
                        EventDb.UpdatePeminjamanDetail(
                          _controllerkodePengajuan.text,
                          _controllerkodeBarang.text,
                          _controllerjumlah.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.peminjaman_detail == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorPrimaryDark,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}