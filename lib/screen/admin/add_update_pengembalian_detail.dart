import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:project_kelas/config/asset.dart';
import 'package:project_kelas/event/event_db.dart';
import 'package:project_kelas/model/pengembalian_detail.dart';
import 'package:project_kelas/model/pengembalian_detail.dart';
import 'package:project_kelas/screen/admin/list_pengembalian_detail.dart';
import 'package:project_kelas/screen/admin/list_pengembalian.dart';
import 'package:project_kelas/widget/info.dart';
import '../../model/pengembalian.dart';

class AddUpdatePengembalianDetail extends StatefulWidget {
  final Pengembalian_detail? pengembalian_detail;
  AddUpdatePengembalianDetail({this.pengembalian_detail});

  @override
  State<AddUpdatePengembalianDetail> createState() => _AddUpdatePengembalianDetailState();
}

class _AddUpdatePengembalianDetailState extends State<AddUpdatePengembalianDetail> {
  var _formKey = GlobalKey<FormState>();
  var _controllerkodePengembalianDetail = TextEditingController();
  var _controllerkodePengembalian = TextEditingController();
  var _controllerkodeBarang = TextEditingController();
  var _controllerjumlah = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.pengembalian_detail != null) {
      _controllerkodePengembalianDetail.text = widget.pengembalian_detail!.kode_pengembalian_detail!;
      _controllerkodePengembalian.text = widget.pengembalian_detail!.kode_pengembalian!;
      _controllerkodeBarang.text = widget.pengembalian_detail!.kode_barang!;
      _controllerjumlah.text = widget.pengembalian_detail!.jumlah!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.pengembalian_detail != null
            ? Text('Update Detail Pengembalian')
            : Text('Tambah Detail Pengembalian'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.pengembalian_detail == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkodePengembalianDetail,
                  decoration: InputDecoration(
                      labelText: "Kode Pengembalian Detail",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkodePengembalian,
                  decoration: InputDecoration(
                      labelText: "Kode Pengembalian",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),       
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkodeBarang,
                  decoration: InputDecoration(
                      labelText: "Kode Barang",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerjumlah,
                  decoration: InputDecoration(
                      labelText: "Jumlah",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.pengembalian_detail == null) {
                        String message = await EventDb.AddPengembalian_detail(
                          _controllerkodePengembalianDetail.text,
                          _controllerkodePengembalian.text,
                          _controllerkodeBarang.text,
                          _controllerjumlah.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerkodePengembalianDetail.clear();
                          _controllerkodePengembalian.clear();
                          _controllerkodeBarang.clear();
                          _controllerjumlah.clear();
                          Get.off(
                            ListPengembalianDetail(),
                          );
                        }
                      } else {
                        EventDb.UpdatePengembalian_detail(
                          _controllerkodePengembalianDetail.text,
                          _controllerkodePengembalian.text,
                          _controllerkodeBarang.text,
                          _controllerjumlah.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.pengembalian_detail == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorPrimaryDark,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}