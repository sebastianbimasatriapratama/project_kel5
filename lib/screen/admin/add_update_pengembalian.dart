import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:project_kelas/config/asset.dart';
import 'package:project_kelas/event/event_db.dart';
import 'package:project_kelas/screen/admin/list_pengembalian.dart';
import 'package:project_kelas/widget/info.dart';

import '../../model/pengembalian.dart';

class AddUpdatePengembalian extends StatefulWidget {
  final Pengembalian? pengembalian;
  AddUpdatePengembalian({this.pengembalian});

  @override
  State<AddUpdatePengembalian> createState() => _AddUpdatePengembalianState();
}

class _AddUpdatePengembalianState extends State<AddUpdatePengembalian> {
  var _formKey = GlobalKey<FormState>();
  var _controllerkode_pengembalian = TextEditingController();
  var _controllerkode_pengajuan = TextEditingController();
  var _controllertanggal_kembali = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.pengembalian != null) {
      _controllerkode_pengembalian.text =
          widget.pengembalian!.kode_pengembalian!;
      _controllerkode_pengajuan.text = widget.pengembalian!.kode_pengajuan!;
      _controllertanggal_kembali.text = widget.pengembalian!.tanggal_kembali!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.pengembalian != null
            ? Text('Update Pengembalian')
            : Text('Tambah Pengembalian'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.pengembalian == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkode_pengembalian,
                  decoration: InputDecoration(
                      labelText: "kode pengambilan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkode_pengajuan,
                  decoration: InputDecoration(
                      labelText: "kode pengajuan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllertanggal_kembali,
                  decoration: InputDecoration(
                      labelText: "tanggal kembali",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.pengembalian == null) {
                        String message = await EventDb.AddPengembalian(
                          _controllerkode_pengembalian.text,
                          _controllerkode_pengajuan.text,
                          _controllertanggal_kembali.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerkode_pengembalian.clear();
                          _controllerkode_pengajuan.clear();
                          _controllertanggal_kembali.clear();
                          Get.off(
                            ListPengembalian(),
                          );
                        }
                      } else {
                        EventDb.UpdatePengembalian(
                          _controllerkode_pengembalian.text,
                          _controllerkode_pengajuan.text,
                          _controllertanggal_kembali.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.pengembalian == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
