import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:project_kelas/config/asset.dart';
import 'package:project_kelas/event/event_db.dart';
import 'package:project_kelas/model/pengembalian_detail.dart';
import 'package:project_kelas/model/pengembalian.dart';
import 'package:project_kelas/screen/admin/add_update_pengembalian.dart';
import 'package:project_kelas/screen/admin/add_update_pengembalian_detail.dart';
import 'package:project_kelas/screen/admin/list_pengembalian.dart';
import 'package:project_kelas/screen/admin/list_user.dart';
// import 'package:project_kelas/screen/admin/add_update_pengembalian.dart';

class ListPengembalianDetail extends StatefulWidget {
  @override
  State<ListPengembalianDetail> createState() => _ListPengembalianDetailState();
}

class _ListPengembalianDetailState extends State<ListPengembalianDetail> {
  List<Pengembalian_detail> _listDetailPengembalian = [];

  void getPengembalianDetail() async {
    _listDetailPengembalian = await EventDb.getPengembalian_detail();

    setState(() {});
  }

  @override
  void initState() {
    getPengembalianDetail();
    super.initState();
  }

  void showOption(Pengembalian_detail? pengembalian_detail) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdatePengembalianDetail(pengembalian_detail: pengembalian_detail))
            ?.then((value) => getPengembalianDetail());
        break;
      case 'delete':
        EventDb.deletePengembalian_detail(pengembalian_detail!.kode_pengembalian_detail!)
            .then((value) => getPengembalianDetail());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        gradient:
            LinearGradient(colors: [Asset.colorPrimary, Asset.colorPrimaryDark]),
        // titleSpacing: 0,
        title: Text('List Detail Pengembalian')
      ),
      body: Stack(
        children: [
          _listDetailPengembalian.length > 0
              ? ListView.builder(
                  itemCount: _listDetailPengembalian.length,
                  itemBuilder: (context, index) {
                    Pengembalian_detail detailPengembalian = _listDetailPengembalian[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(detailPengembalian.kode_barang?? ''),
                      subtitle: Text(detailPengembalian.jumlah ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(detailPengembalian),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () =>
                  Get.to(AddUpdatePengembalianDetail())?.then((value) => getPengembalianDetail()),
              child: Icon(Icons.add),
              backgroundColor: Asset.colorAccent,
            ),
          )
        ],
      ),
    );
  }
}