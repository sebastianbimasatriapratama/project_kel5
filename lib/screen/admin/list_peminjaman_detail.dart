import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:project_kelas/config/asset.dart';
import 'package:project_kelas/event/event_db.dart';
import 'package:project_kelas/model/peminjaman_detail.dart';
import 'package:project_kelas/model/peminjaman.dart';
import 'package:project_kelas/screen/admin/add_update_peminjaman.dart';
import 'package:project_kelas/screen/admin/add_update_peminjaman_detail.dart';
import 'package:project_kelas/screen/admin/list_peminjaman.dart';
import 'package:project_kelas/screen/admin/list_user.dart';
// import 'package:project_kelas/screen/admin/add_update_peminjaman.dart';

class ListPeminjamanDetail extends StatefulWidget {
  @override
  State<ListPeminjamanDetail> createState() => _ListPeminjamanDetailState();
}

class _ListPeminjamanDetailState extends State<ListPeminjamanDetail> {
  List<Peminjaman_detail> _listDetailPeminjaman = [];

  void getPeminjamanDetail() async {
    _listDetailPeminjaman = await EventDb.getPeminjamanDetail();

    setState(() {});
  }

  @override
  void initState() {
    getPeminjamanDetail();
    super.initState();
  }

  void showOption(Peminjaman_detail? peminjaman_detail) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdatePeminjamanDetail(peminjaman_detail: peminjaman_detail))
            ?.then((value) => getPeminjamanDetail());
        break;
      case 'delete':
        EventDb.deletePeminjamanDetail(peminjaman_detail!.kode_pengajuan!)
            .then((value) => getPeminjamanDetail());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        gradient:
            LinearGradient(colors: [Asset.colorPrimary, Asset.colorPrimaryDark]),
        // titleSpacing: 0,
        title: Text('List Detail Peminjaman')
      ),
      body: Stack(
        children: [
          _listDetailPeminjaman.length > 0
              ? ListView.builder(
                  itemCount: _listDetailPeminjaman.length,
                  itemBuilder: (context, index) {
                    Peminjaman_detail detailPeminjaman = _listDetailPeminjaman[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(detailPeminjaman.kode_barang?? ''),
                      subtitle: Text(detailPeminjaman.jumlah ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(detailPeminjaman),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () =>
                  Get.to(AddUpdatePeminjamanDetail())?.then((value) => getPeminjamanDetail()),
              child: Icon(Icons.add),
              backgroundColor: Asset.colorAccent,
            ),
          )
        ],
      ),
    );
  }
}