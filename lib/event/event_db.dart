import 'dart:convert';
import 'package:get/get.dart';
import 'package:project_kelas/config/api.dart';
import 'package:project_kelas/event/event_pref.dart';
import 'package:project_kelas/model/mahasiswa.dart';
import 'package:project_kelas/model/peminjaman.dart';
import 'package:project_kelas/model/peminjaman_detail.dart';
import 'package:project_kelas/model/user.dart';
import 'package:project_kelas/model/barang.dart';
import 'package:project_kelas/model/pengembalian.dart';
import 'package:http/http.dart' as http;
import 'package:project_kelas/screen/admin/dashboard_admin.dart';
import 'package:project_kelas/screen/login.dart';
import 'package:project_kelas/widget/info.dart';

import '../model/pengembalian_detail.dart';

class EventDb {
  static Future<User?> login(String username, String pass) async {
    User? user;

    try {
      var response = await http.post(Uri.parse(Api.login), body: {
        'username': username,
        'pass': pass,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);

        if (responBody['success']) {
          user = User.fromJson(responBody['user']);
          EventPref.saveUser(user);
          Info.snackbar('Login Berhasil');
          Future.delayed(Duration(milliseconds: 1700), () {
            Get.off(
              DashboardAdmin(),
            );
          });
        } else {
          Info.snackbar('Login Gagal');
        }
      } else {
        Info.snackbar('Request Login Gagal');
      }
    } catch (e) {
      print(e);
    }
    return user;
  }

  static Future<List<User>> getUser() async {
    List<User> listUser = [];

    try {
      var response = await http.get(Uri.parse(Api.getUsers));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var users = responBody['user'];

          users.forEach((user) {
            listUser.add(User.fromJson(user));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listUser;
  }

  static Future<String> addUser(
      String name, String username, String pass, String role) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addUser), body: {
        'name': name,
        'username': username,
        'pass': pass,
        'role': role
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add User Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdateUser(
    String id,
    String name,
    String username,
    String pass,
    String role,
  ) async {
    try {
      var response = await http.post(Uri.parse(Api.updateUser), body: {
        'id': id,
        'name': name,
        'username': username,
        'pass': pass,
        'role': role
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update User');
        } else {
          Info.snackbar('Gagal Update User');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deleteUser(String id) async {
    try {
      var response =
          await http.post(Uri.parse(Api.deleteUser), body: {'id': id});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete User');
        } else {
          Info.snackbar('Gagal Delete User');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<List<Mahasiswa>> getMahasiswa() async {
    List<Mahasiswa> listMahasiswa = [];

    try {
      var response = await http.get(Uri.parse(Api.getMahasiswa));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var mahasiswa = responBody['mahasiswa'];

          mahasiswa.forEach((mahasiswa) {
            listMahasiswa.add(Mahasiswa.fromJson(mahasiswa));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listMahasiswa;
  }

  static Future<String> AddMahasiswa(String mhsNpm, String mhsNama,
      String mhsAlamat, String mhsFakultas, String MhsProdi) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addMahasiswa), body: {
        'mhsNpm': mhsNpm,
        'mhsNama': mhsNama,
        'mhsAlamat': mhsAlamat,
        'mhsFakultas': mhsFakultas,
        'MhsProdi': MhsProdi,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Mahasiswa Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdateMahasiswa(String mhsNpm, String mhsNama,
      String mhsAlamat, String mhsFakultas, String MhsProdi) async {
    try {
      var response = await http.post(Uri.parse(Api.updateMahasiswa), body: {
        'mhsNpm': mhsNpm,
        'mhsNama': mhsNama,
        'mhsAlamat': mhsAlamat,
        'mhsFakultas': mhsFakultas,
        'MhsProdi': MhsProdi
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Mahasiswa');
        } else {
          Info.snackbar('Gagal Update Mahasiswa');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deleteMahasiswa(String mhsNpm) async {
    try {
      var response = await http
          .post(Uri.parse(Api.deleteMahasiswa), body: {'mhsNpm': mhsNpm});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Mahasiswa');
        } else {
          Info.snackbar('Gagal Delete Mahasiswa');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<List<Barang>> getBarang() async {
    List<Barang> listBarang = [];

    try {
      var response = await http.get(Uri.parse(Api.getBarang));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var barang = responBody['barang'];

          barang.forEach((barang) {
            listBarang.add(Barang.fromJson(barang));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listBarang;
  }

  static Future<String> AddBarang(
      String kode_barang, String nama_barang, String jumlah) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addBarang), body: {
        'kode_barang': kode_barang,
        'nama_barang': nama_barang,
        'jumlah': jumlah,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Barang Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdateBarang(
      String kode_barang, String nama_barang, String jumlah) async {
    try {
      var response = await http.post(Uri.parse(Api.updateBarang), body: {
        'kode_barang': kode_barang,
        'nama_barang': nama_barang,
        'jumlah': jumlah,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Barang');
        } else {
          Info.snackbar('Gagal Update Barang');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deleteBarang(String kode_barang) async {
    try {
      var response = await http.post(Uri.parse(Api.deleteBarang),
          body: {'kode_barang': kode_barang});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Barang');
        } else {
          Info.snackbar('Gagal Delete Barang');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<List<Peminjaman>> getPeminjaman() async {
    List<Peminjaman> listPeminjaman = [];

    try {
      var response = await http.get(Uri.parse(Api.getPeminjaman));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var peminjaman = responBody['peminjaman'];

          peminjaman.forEach((peminjaman) {
            listPeminjaman.add(Peminjaman.fromJson(peminjaman));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listPeminjaman;
  }

  static Future<String> AddPeminjaman(
      String npm_peminjam,
      String kode_pengajuan,
      String tanggal,
      String nama_peminjam,
      String prodi) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addPeminjaman), body: {
        'npm_peminjam': npm_peminjam,
        'kode_pengajuan': kode_pengajuan,
        'tanggal': tanggal,
        'nama_peminjam': nama_peminjam,
        'prodi': prodi,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Peminjaman Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdatePeminjaman(
      String npm_peminjam,
      String kode_peminjaman,
      String tanggal,
      String nama_peminjam,
      String prodi) async {
    try {
      var response = await http.post(Uri.parse(Api.updatePeminjaman), body: {
        'npm_peminjam': npm_peminjam,
        'kode_peminjaman': kode_peminjaman,
        'tanggal': tanggal,
        'nama_peminjam': nama_peminjam,
        'prodi': prodi,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Peminjaman');
        } else {
          Info.snackbar('Gagal Update Peminjaman');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deletePeminjaman(String npm_peminjam) async {
    try {
      var response = await http.post(Uri.parse(Api.deletePeminjaman),
          body: {'npm_peminjam': npm_peminjam});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Peminjaman');
        } else {
          Info.snackbar('Gagal Delete Peminjaman');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<List<Peminjaman_detail>> getPeminjamanDetail() async {
    List<Peminjaman_detail> listPeminjamanDetail = [];

    try {
      var response = await http.get(Uri.parse(Api.getPeminjaman_detail));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var peminjaman_detail = responBody['peminjaman_detail'];

          peminjaman_detail.forEach((peminjaman_detail) {
            listPeminjamanDetail
                .add(Peminjaman_detail.fromJson(peminjaman_detail));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listPeminjamanDetail;
  }

  static Future<String> AddPeminjamanDetail(
    String kodePengajuan,
    String kodeBarang,
    String jumlah,
  ) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addPeminjaman_detail),
          body: {
            'kode_pengajuan': kodePengajuan,
            'kode_barang': kodeBarang,
            'jumlah': jumlah
          });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Peminjaman Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdatePeminjamanDetail(
    String kodePengajuan,
    String kodeBarang,
    String jumlah,
  ) async {
    try {
      var response = await http.post(Uri.parse(Api.updatePeminjaman_detail),
          body: {
            'kode_pengajuan': kodePengajuan,
            'kode_barang': kodeBarang,
            'jumlah': jumlah
          });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Peminjaman');
        } else {
          Info.snackbar('Gagal Update Peminjaman');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deletePeminjamanDetail(String kodePengajuan) async {
    try {
      var response = await http.post(Uri.parse(Api.deletePeminjaman_detail),
          body: {'kode_pengajuan': kodePengajuan});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Peminjaman');
        } else {
          Info.snackbar('Gagal Delete Peminjaman');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<List<Pengembalian>> getPengembalian() async {
    List<Pengembalian> listPengembalian = [];

    try {
      var response = await http.get(Uri.parse(Api.getPengembalian));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var pengembalian = responBody['pengembalian'];

          pengembalian.forEach((pengembalian) {
            listPengembalian.add(Pengembalian.fromJson(pengembalian));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listPengembalian;
  }

  static Future<String> AddPengembalian(String kode_pengembalian,
      String kode_pengajuan, String tanggal_kembali) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addPengembalian), body: {
        'kode_pengembalian': kode_pengembalian,
        'kode_pengajuan': kode_pengajuan,
        'tanggal_kembali': tanggal_kembali,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Pengembalian Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdatePengembalian(String kode_pengembalian,
      String kode_pengajuan, String tanggal_kembali) async {
    try {
      var response = await http.post(Uri.parse(Api.updatePengembalian), body: {
        'kode_pengembalian': kode_pengembalian,
        'kode_pengajuan': kode_pengajuan,
        'tanggal_kembali': tanggal_kembali,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Pengembalian');
        } else {
          Info.snackbar('Gagal Update Pengembalian');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deletePengembalian(String kode_pengembalian) async {
    try {
      var response = await http.post(Uri.parse(Api.deletePengembalian),
          body: {'kode_pengembalian': kode_pengembalian});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Pengembalian');
        } else {
          Info.snackbar('Gagal Delete Pengembalian');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<List<Pengembalian_detail>> getPengembalian_detail() async {
    List<Pengembalian_detail> listPengembalian_detail = [];

    try {
      var response = await http.get(Uri.parse(Api.getPengembalian_detail));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var pengembalian_detail = responBody['pengembalian_detail'];

          pengembalian_detail.forEach((pengembalian_detail) {
            listPengembalian_detail
                .add(Pengembalian_detail.fromJson(pengembalian_detail));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listPengembalian_detail;
  }

  static Future<String> AddPengembalian_detail(String kode_pengembalian_detail,
      String kode_pengembalian, String kode_barang, String jumlah) async {
    String reason;

    try {
      var response =
          await http.post(Uri.parse(Api.addPengembalian_detail), body: {
        'kode_pengembalian_detail': kode_pengembalian_detail,
        'kode_pengembalian': kode_pengembalian,
        'kode_barang': kode_barang,
        'jumlah': jumlah,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Pengembalian_detail Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdatePengembalian_detail(String kode_pengembalian_detail,
      String kode_pengembalian, String kode_barang, String jumlah) async {
    try {
      var response =
          await http.post(Uri.parse(Api.updatePengembalian_detail), body: {
        'kode_pengembalian_detail': kode_pengembalian_detail,
        'kode_pengembalian': kode_pengembalian,
        'kode_barang': kode_barang,
        'jumlah': jumlah,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Pengembalian_detail');
        } else {
          Info.snackbar('Gagal Update Pengembalian_detail');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deletePengembalian_detail(
      String kode_pengembalian_detail) async {
    try {
      var response = await http.post(Uri.parse(Api.deletePengembalian_detail),
          body: {'kode_pengembalian_detail': kode_pengembalian_detail});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Pengembalian_detail');
        } else {
          Info.snackbar('Gagal Delete Pengembalian_detail');
        }
      }
    } catch (e) {
      print(e);
    }
  }
}
