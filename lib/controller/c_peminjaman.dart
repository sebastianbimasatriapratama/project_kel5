import 'package:get/get.dart';
import 'package:project_kelas/model/peminjaman.dart';

class CPeminjaman extends GetxController {
  Rx<Peminjaman> _peminjaman = Peminjaman().obs;

  Peminjaman get user => _peminjaman.value;

  void setUser(Peminjaman dataPeminjaman) => _peminjaman.value = dataPeminjaman;
}
