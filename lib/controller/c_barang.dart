import 'package:get/get.dart';
import 'package:project_kelas/model/barang.dart';

class CBarang extends GetxController {
  Rx<Barang> _barang = Barang().obs;

  Barang get user => _barang.value;

  void setUser(Barang dataBarang) => _barang.value = dataBarang;
}
