import 'package:get/get.dart';
import 'package:project_kelas/model/peminjaman_detail.dart';

class CPeminjaman_detail extends GetxController {
  Rx<Peminjaman_detail> _peminjaman_detail = Peminjaman_detail().obs;

  Peminjaman_detail get user => _peminjaman_detail.value;

  void setUser(Peminjaman_detail dataPeminjaman_detail) =>
      _peminjaman_detail.value = dataPeminjaman_detail;
}
