import 'package:get/get.dart';
import 'package:project_kelas/model/pengembalian_detail.dart';

class CPengembalian extends GetxController {
  Rx<Pengembalian_detail> _pengembalian_detail = Pengembalian_detail().obs;

  Pengembalian_detail get user => _pengembalian_detail.value;

  void setUser(Pengembalian_detail dataPengembalian_detail) =>
      _pengembalian_detail.value = dataPengembalian_detail;
}
